/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import Header from './Header';
import Navigation from './Navigation';
import s from './Layout.css';
import {Provider,connect} from 'react-redux';

function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    login: (acc_email,acc_pass) => {
      if(typeof(acc_email)=="undefined"||typeof(acc_pass)=="undefined")
        dispatch({type:"LOGIN_ERR",msg:"Not enough params"});
      fetch("https://api.voximplant.com/platform_api/Logon/?" +
        "account_email="+acc_email+
        "&account_password="+acc_pass)
        .then((response)=>{
          return response.text();
        })
        .then((body)=>{
          let result = JSON.parse(body);
          if(typeof(result.error)!="undefined"){
            dispatch({type:"LOGIN_ERR",msg:result.error.msg});
          }else{
            dispatch({type:"LOGIN_OK",
              result:result.result,
              api_key:result.api_key,
              account_id:result.account_id,
              balance:result.balance})
          }
        }).catch(()=>{
        dispatch({type:"LOGIN_ERR",msg:"Error in HTTP request"});
      });
    }
  }
};


class Layout extends React.Component {
  store = {};
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      login:'',
      pass:'',
      auth_error:false,
      auth_msg:''
    };
  }
  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  render() {
    this.store = this._reactInternalInstance._context.store;
    window.addEventListener('storage', (e)=>{
      if(e.key=='cache'){
        this.store.dispatch({type:"EDITED_OTHER",value:JSON.parse(e.newValue)});
      }
    });
    let ret = {};
    if(this.props.auth)
      ret = <main className="mdl-layout__content">
        <div {...this.props}  className={s.content}/>
        </main>;
    else
      ret = <main className={s.content}>
        <div className="mdl-grid">
          <div className="mdl-cell mdl-cell--2-col-desktop mdl-cell--8-col-tablet mdl-cell--12-col-phone mdl-cell--5-offset-desktop mdl-cell--2-offset-tablet">
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="login"
                     value={this.state.login}
                     onChange={(e) => { this.setState({login:e.target.value});}}/>
              <label className="mdl-textfield__label" htmlFor="login">Login</label>
            </div>
            <div className={((this.props.auth_error)?'is-invalid ':'')+((this.state.pass.length)?'is-dirty ':'')+"mdl-textfield mdl-js-textfield mdl-textfield--floating-label"}>
              <input className="mdl-textfield__input"
                     type="password"
                     id="pwd"
                     value={this.state.pass}
                     onChange={(e) => { this.setState({pass:e.target.value});}}/>
              <label className="mdl-textfield__label" htmlFor="pwd">Password</label>
              <span className="mdl-textfield__error">{this.props.auth_msg}</span>
            </div>
            <button className="mdl-button mdl-js-button mdl-button--raised mdl-button--colored"
            onClick={()=>{this.props.login(this.state.login,this.state.pass)}}>
              Login
            </button>
          </div>
        </div>
      </main>;
    return (
      <Provider store={this.store}>
        <div className="mdl-layout mdl-js-layout mdl-layout--fixed-header" ref="root">
          <div className="mdl-layout__inner-container">
            <Header />
            <div className="mdl-layout__drawer">
              <span className="mdl-layout-title">SubAcc Manager</span>
              <Navigation/>
            </div>
            {ret}
          </div>
        </div>
      </Provider>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
