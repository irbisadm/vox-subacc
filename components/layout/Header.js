/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { connect } from 'react-redux';

function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    balance:state.balance,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {}
};

class Header extends React.Component {

  componentDidMount() {
    window.componentHandler.upgradeElement(this.refs.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  render() {
    return (
      <header className="mdl-layout__header" ref="root">
        <div className="mdl-layout__header-row">
          <span className="mdl-layout-title">VoxImplant ChildAccounts Manager</span>
          <div className="mdl-layout-spacer"></div>
          <div className="balance">{(this.props.auth)?this.props.balance:""}</div>
        </div>
      </header>

    );
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
