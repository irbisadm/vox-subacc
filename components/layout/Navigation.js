/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { Link } from 'react-app';
import { connect } from 'react-redux';


function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    balance:state.balance,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {

    logout:(account_id,session_id)=>{
      fetch("https://api.voximplant.com/platform_api/Logout/?" +
        "account_id="+account_id+
        "&session_id="+session_id)
        .then((response)=>{
          dispatch({type:"LOGOUT"})
        })
    }
  }
};

class Navigation extends React.Component {

  componentDidMount() {
    window.componentHandler.upgradeElement(this.refs.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  render() {
    let menu = <nav className="mdl-navigation" ref="root">
      <Link className="mdl-navigation__link" to="/">Home</Link>
      <Link className="mdl-navigation__link" to="/about">About</Link>
    </nav>;
    if(this.props.auth)
      menu = <nav className="mdl-navigation" ref="root">
        <Link className="mdl-navigation__link" to="/">Home</Link>
        <a className="mdl-navigation__link" onClick={()=>{this.props.logout(this.props.account_id,this.props.session_id)}}>Logout</a>
        <Link className="mdl-navigation__link" to="/about">About</Link>
      </nav>;

    return menu;
  }

}

export default connect(mapStateToProps, mapDispatchToProps)(Navigation);
