/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './SubAccTable.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';
import {FabButtonBlock} from '../material/FabButtonBlock';

function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    logouted:()=>{
      dispatch({type:"LOGOUT"})
    }
  }
};


class SubAccTable extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: true,
      subacc: [],
      page:1,
      totall_pages:0
    }
  }
  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
    fetch("https://api.voximplant.com/platform_api/GetChildrenAccounts/?" +
      "account_id="+this.props.account_id+
      "&session_id="+this.props.session_id)
      .then((response)=>{
        return response.text();
      })
      .then((body)=>{
        let response = JSON.parse(body);
        if(typeof(response.error)!="undefined"){
          if(response.error.code==100){
            this.props.logouted();
          }
        }else{
          this.setState({loading:false,subacc:response.result,totall_pages:Math.ceil(response.total_count/20)})
        }
      })
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  render(){
    let loading = "";
    let items = "";
    if(this.state.loading)
      loading = <div id="progressbar" className="mdl-progress mdl-js-progress mdl-progress__indeterminate" style={{width:"100%"}}></div>;
    if(this.state.subacc.length>0)
      items = this.state.subacc.map((result,index)=>{
        return <tr key={result.account_id}>
          <td className="mdl-data-table__cell--non-numeric"><Link to={'/show/'+result.account_id}>{result.account_id}</Link></td>
          <td className="mdl-data-table__cell--non-numeric">{result.account_name}</td>
          <td className="mdl-data-table__cell--non-numeric">{result.account_email}</td>
          <td className="mdl-data-table__cell--non-numeric">{result.created}</td>
          <td>{result.balance}</td>
        </tr>
      });
    else
      items = <tr><td colSpan="5" style={{textAlign:"center"}}>Sorry, you have't any child accounts.</td></tr>;
    return (<div ref="root">
      {loading}
      <table className="mdl-data-table mdl-js-data-table mdl-shadow--2dp" style={{width:"100%"}}>
        <thead>
        <tr>
          <th className="mdl-data-table__cell--non-numeric">ID</th>
          <th className="mdl-data-table__cell--non-numeric">Account's name</th>
          <th className="mdl-data-table__cell--non-numeric">Account's email</th>
          <th className="mdl-data-table__cell--non-numeric">Created time</th>
          <th>Balance</th>
        </tr>
        </thead>
        <tbody>
        {items}
        </tbody>
      </table>
      <FabButtonBlock buttons={{icon:'add',nav:'/add'}}/>
    </div>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubAccTable);
