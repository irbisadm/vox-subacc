/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './ShowHistory.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';


class ShowHistory extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: true,
      page: 0,
      totalPages:1,
      from_date:'',
      to_date:'',
      timezone:'auto',
      application_name:'',
      application_id:'',
      call_session_history_id:'',
      user_id:'',
      rule_name:'',
      remote_number:'',
      local_number:'',
      call_session_history_custom_data:'',
      with_calls:false,
      with_records:false,
      with_other_resources:true,
      desc_order:false,
      historyRecords:[]
    }
  }

  static propTypes = {
    clientId:React.PropTypes.number.isRequired,
    apiKey:React.PropTypes.string.isRequired
  };

  componentDidMount() {
    this.getData();
    window.componentHandler.upgradeElements(this.refs.root);
  }

  componentWillUnmount(){
    window.componentHandler.downgradeElements(this.refs.root);
  }

  getData(){
    fetch("https://api.voximplant.com/platform_api/GetCallHistory/",{
        method: 'post',
        headers: {
          "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
        },
        body:"account_id=" + this.props.clientId +
              "&api_key=" + this.props.apiKey +
              "&count=500"+
              "&offset="+(20*this.state.page)+
              this.filterRequestParams()
      }
    )
      .then((response)=>{
        return response.text();
      })
      .then((body)=> {
        let response = JSON.parse(body);
        if(typeof (response.result)!="undefined"){
          this.setState({loading:false,historyRecords:response.result,totalPages:(Math.ceil(response.total_count/500)),timezone:response.timezone});
        }
      })
  }

  *getData2(filters=""){
    let page=0;
    let perPage = 500;
    let maxpage = Number.POSITIVE_INFINITY;
    let offset = 0;
    while (page<maxpage){
      let fromstart = perPage*page+1;
      offset = perPage*page;
      yield fetch("https://api.voximplant.com/platform_api/GetCallHistory/",{
          method: 'post',
          headers: {
            "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"
          },
          body:"account_id=" + this.props.clientId +
          "&api_key=" + this.props.apiKey +
          "&count="+ perPage +
          "&offset="+offset+
          filters
        }
      ).then((response)=>{return response.text();})
        .then((body)=> {
          let response = JSON.parse(body);
          if(typeof (response.result)!="undefined"){
            maxpage = Math.ceil(response.total_count/perPage);
            this.setState({loading:false,historyRecords:response.result});
            page++;
            return {from:fromstart,to:fromstart+response.count};
          }else{
            return response;
          }
        })
    }
    return page;
  };

  filterRequestParams(){
    let requestParam ="";
    if(this.state.from_date.length>0)
      requestParam+= "&from_date="+this.state.from_date;
    if(this.state.to_date.length>0)
      requestParam+= "&to_date="+this.state.to_date;
    if(this.state.timezone.length>0)
      requestParam+= "&timezone="+this.state.timezone;
    if(this.state.application_name.length>0)
      requestParam+= "&application_name="+this.state.application_name;
    if(this.state.application_id.length>0)
      requestParam+= "&application_id="+this.state.application_id;
    if(this.state.call_session_history_id.length>0)
      requestParam+= "&call_session_history_id="+this.state.call_session_history_id;
    if(this.state.user_id.length>0)
      requestParam+= "&user_id="+this.state.user_id;
    if(this.state.rule_name.length>0)
      requestParam+= "&rule_name="+this.state.rule_name;
    if(this.state.remote_number.length>0)
      requestParam+= "&remote_number="+this.state.remote_number;
    if(this.state.local_number.length>0)
      requestParam+= "&local_number="+this.state.local_number;
    if(this.state.call_session_history_custom_data.length>0)
      requestParam+= "&call_session_history_custom_data="+this.state.call_session_history_custom_data;
    return requestParam;
  }

  render(){
    return(<div ref="root">
      <div className="mdl-grid">
        <div className="mdl-cell mdl-cell--10-col mdl-cell--8-col-tablet mdl-cell--12-col-phone mdl-card mdl-shadow--2dp">
          <table className={s.mainTable+" mdl-data-table mdl-js-data-table"}>
            <thead>
            <tr>
              <th>ID</th>
              <th>ApplicationID</th>
              <th>UserID</th>
              <th className="mdl-data-table__cell--non-numeric">Start Date</th>
              <th className="mdl-data-table__cell--non-numeric">Duration</th>
              <th className="mdl-data-table__cell--non-numeric">Finish Reason</th>
              <th className="mdl-data-table__cell--non-numeric">Initiator</th>
              <th className="mdl-data-table__cell--non-numeric">MS</th>
              <th className="mdl-data-table__cell--non-numeric">Action</th>
            </tr>
            </thead>
            <tbody>
            {()=>{
              this.state.historyRecords.map((item,idx)=>{
                return <tr>
                    <td>{item.call_session_history_id}</td>
                    <td>{item.application_id}</td>
                    <td>{item.user_id}</td>
                    <td className="mdl-data-table__cell--non-numeric">{item.start_date}</td>
                    <td className="mdl-data-table__cell--non-numeric">{(item.duration!="undefined")?item.duration:'N/A'}</td>
                    <td className="mdl-data-table__cell--non-numeric">{(item.finish_reason!="undefined")?item.finish_reason:'N/A'}</td>
                    <td className="mdl-data-table__cell--non-numeric">{item.initiator_address}</td>
                    <td className="mdl-data-table__cell--non-numeric">{item.media_server_address}</td>
                    <td className="mdl-data-table__cell--non-numeric">
                      <a href={item.log_file_url} className="mdl-button mdl-js-button mdl-button--icon mdl-button--colored" target="_blank">
                        <i class="material-icons">history</i>
                      </a>
                    </td>
                  </tr>
              })
            }}

            </tbody>
          </table>
          <div className={s.spinner}>
            <div className={"mdl-spinner mdl-js-spinner "+(this.state.loading?"is-active":"")}></div>
          </div>
        </div>
        <div className="mdl-cell mdl-cell--2-col mdl-cell--4-col-tablet mdl-cell--12-col-phone mdl-card mdl-shadow--2dp">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Filter</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="from_date-input"
                     pattern="[1-2][0-9]{3}-[0-1][0-9]-[0-3][0-9]\s[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"
                     value={this.state.from_date}
                     onChange={(e)=>{this.setState({from_date:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="from_date-input">From date</label>
              <span className="mdl-textfield__error">The date in the selected timezone in format YYYY-MM-DD HH:mm:SS!</span>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="to_date-input"
                     pattern="[1-2][0-9]{3}-[0-1][0-9]-[0-3][0-9]\s[0-2][0-9]:[0-5][0-9]:[0-5][0-9]"
                     value={this.state.to_date}
                     onChange={(e)=>{this.setState({to_date:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="to_date-input">To date</label>
              <span className="mdl-textfield__error">The date in the selected timezone in format YYYY-MM-DD HH:mm:SS!</span>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="timezone-input"
                     value={this.state.timezone}
                     onChange={(e)=>{this.setState({timezone:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="timezone-input">Timezone</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="call_session_history_id-input"
                     value={this.state.call_session_history_id}
                     onChange={(e)=>{this.setState({call_session_history_id:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="call_session_history_id-input">Call session history id</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="application_id-input"
                     value={this.state.application_id}
                     onChange={(e)=>{this.setState({application_id:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="application_id-input">Application id</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="application_name-input"
                     value={this.state.application_name}
                     onChange={(e)=>{this.setState({application_name:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="application_name-input">Application name</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="user_id-input"
                     value={this.state.user_id}
                     onChange={(e)=>{this.setState({user_id:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="user_id-input">User id</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="rule_name-input"
                     value={this.state.rule_name}
                     onChange={(e)=>{this.setState({rule_name:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="rule_name-input">Rule name</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="remote_number-input"
                     value={this.state.remote_number}
                     onChange={(e)=>{this.setState({remote_number:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="remote_number-input">Remote number</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="local_number-input"
                     value={this.state.local_number}
                     onChange={(e)=>{this.setState({local_number:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="local_number-input">Local number</label>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
              <input className="mdl-textfield__input"
                     type="text"
                     id="call_session_history_custom_data-input"
                     value={this.state.call_session_history_custom_data}
                     onChange={(e)=>{this.setState({call_session_history_custom_data:e.target.value})}}
              />
              <label className="mdl-textfield__label" htmlFor="call_session_history_custom_data-input">Call session history custom data</label>
            </div>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="with_calls-input">
              <input type="checkbox"
                     id="with_calls-input"
                     className="mdl-switch__input"
                     checked={this.state.with_calls}
                     onChange={(e)=>{this.setState({with_calls:e.target.checked})}}
              />
              <span className="mdl-switch__label">With calls</span>
            </label>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="with_records-input">
              <input type="checkbox"
                     id="with_records-input"
                     className="mdl-switch__input"
                     checked={this.state.with_records}
                     onChange={(e)=>{this.setState({with_records:e.target.checked})}}
              />
              <span className="mdl-switch__label">With records</span>
            </label>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="with_other_resources-input">
              <input type="checkbox"
                     id="with_other_resources-input"
                     className="mdl-switch__input"
                     checked={this.state.with_other_resources}
                     onChange={(e)=>{this.setState({with_other_resources:e.target.checked})}}
              />
              <span className="mdl-switch__label">With other resources</span>
            </label>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="desc_order-input">
              <input type="checkbox"
                     id="desc_order-input"
                     className="mdl-switch__input"
                     checked={this.state.desc_order}
                     onChange={(e)=>{this.setState({desc_order:e.target.checked})}}
              />
              <span className="mdl-switch__label">DESC order</span>
            </label>
          </div>
          <div className="mdl-card__actions mdl-card--border">
            <button className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onClick={()=>{this.getData()}}>
              Apply filter
            </button>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default ShowHistory;
