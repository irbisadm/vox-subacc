/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './SubAccShow.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';
import {FabButtonBlock} from '../material/FabButtonBlock';
import {ShowHistory} from '../../components';
import {ShowNumbers} from '../../components';
import {ShowDocuments} from '../../components';
import {ShowUsers} from '../../components';
import {ShowAIPs} from '../../components';
import {ShowScenarios} from '../../components';

function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    balance: (newBalance) => {
      dispatch({type:"BALANCE_UPD",
        balance:newBalance});
    },
    logouted:()=>{
      dispatch({type:"LOGOUT"})
    }
  }
};


class SubAccShow extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: true,
      account:{},
      transferAmmount:0
    }
  }
  static propTypes = {
    child_id:React.PropTypes.string.isRequired
  };
  componentDidMount() {

    this.updateData();
  }
  updateData(){
    fetch("https://api.voximplant.com/platform_api/GetChildrenAccounts/?"+
      "account_id=" + this.props.account_id +
      "&session_id=" + this.props.session_id+
      "&child_account_id="+this.props.child_id
    )
      .then((response)=>{
        return response.text();
      })
      .then((body)=> {
        let response = JSON.parse(body);
        if(typeof(response.error)!="undefined"){
          if(response.error.code==100){
            this.props.logouted();
          }
        }else{
          if(typeof (response.result)!="undefined"&&response.result.length==1){
            this.setState({loading:false,account:response.result[0]});
            window.componentHandler.upgradeElements(this.refs.root);
          }
        }
      });
  }
  transferMoney(factor){
    this.setState({loading:true});
    let transwerValue = parseInt(this.state.transferAmmount)*factor;
    if(isNaN(transwerValue)){
      window.document.querySelector('#action-toast').MaterialSnackbar.showSnackbar({message: 'Transfer money to child account failed '+this.state.transferAmmount+' not a number'});
      this.setState({loading:false});
      return;
    }
    fetch("https://api.voximplant.com/platform_api/TransferMoneyToChildAccount/?"+
      "account_id=" + this.props.account_id +
      "&session_id=" + this.props.session_id+
      "&child_account_id="+this.props.child_id+
      "&amount="+transwerValue.toFixed(2)
    )
      .then((response)=>{
        return response.text();
      })
      .then((body)=> {
        let response = JSON.parse(body);
        if(typeof response.error=="undefined"){
          this.props.balance(response.balance);
          this.updateData();
          window.document.querySelector('#action-toast').MaterialSnackbar.showSnackbar({message: 'Transfer money '+((factor>0)?'to':'from')+' child account successful'});
        } else{
          this.setState({loading:false});
          window.document.querySelector('#action-toast').MaterialSnackbar.showSnackbar({message: 'Transfer money to child account failed '+response.error.msg});
        }
      });
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }


  render(){
    let state = <i className={s.okc+" material-icons"}>check</i>;
    if(this.state.account.frozen)
      state = <i className={s.errc+"material-icons"}>error</i>;
    if(this.state.account.active==false)
      state = <i className={s.pausec+"material-icons"}>pause</i>;
    let ctx ='';
    if(this.state.loading)
      ctx = <div id="progressbar" className="mdl-progress mdl-js-progress mdl-progress__indeterminate" style={{width:"100%"}}></div>;
    else
      ctx = <div className="mdl-grid">
        <Link to="/" className={s.breadcrumb+" mdl-cell mdl-cell--12-col"}>Back to child account list</Link>
        <div className="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--8-col mdl-cell--12-col-phone">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">{this.state.account.account_name}</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div className={s.valblock}>
              api_key <span className={s.datavalue}>{this.state.account.api_key}</span>
            </div>
            <p><strong>Custom data</strong></p>
            {(typeof this.state.account.account_custom_data!="undefined"&&this.state.account.account_custom_data.length)?this.state.account.account_custom_data:"Custom data not set."}
          </div>
          <div className="mdl-card__actions mdl-card--border">
            <a className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" href={"https://manage.voximplant.com/login.php?email="+this.state.account.account_email+"&apikey="+this.state.account.api_key} target="_blank">
              Login to panel
            </a>
          </div>
          <div className="mdl-card__menu">
            {state}
          </div>
        </div>
        <div className="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-phone">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Money transfer</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div>
              <div className={s.transfer+" mdl-textfield mdl-js-textfield mdl-textfield--floating-label"}>
                <input className="mdl-textfield__input" type="text" pattern="[0-9]*(\.[0-9]+)?" id="transfer_ammount" value={this.state.transferAmmount} onChange={(e)=>{this.setState({transferAmmount:e.target.value})}}/>
                <label className="mdl-textfield__label" htmlFor="transfer_ammount">Transfer ammount</label>
                <span className="mdl-textfield__error">Input is not a number!</span>
              </div>
            </div>
            <br/>
          </div>
          <div className="mdl-card__actions mdl-card--border">
              <a className="mdl-button mdl-button--raised  mdl-js-button mdl-js-ripple-effect" onClick={()=>{this.transferMoney(-1)}} style={{marginRight:"10px",marginTop:"10px"}}>
                Do Transfer From
              </a>
              <a className="mdl-button mdl-button--raised mdl-button--accent mdl-js-button mdl-js-ripple-effect" onClick={()=>{this.transferMoney(1)}} style={{marginTop:"10px"}}>
                Do Transfer To
              </a>
          </div>
        </div>
        <div className="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-phone">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Information</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div className={s.valblock}>
              account_email <span className={s.datavalue}>{this.state.account.account_email}</span>
            </div>
            <div className={s.valblock}>
              first_name <span className={s.datavalue}>{this.state.account.account_first_name}</span>
            </div>
            <div className={s.valblock}>
              last_name <span className={s.datavalue}>{this.state.account.account_last_name}</span>
            </div>
            <div className={s.valblock}>
              language_code <span className={s.datavalue}>{this.state.account.language_code}</span>
            </div>
            <div className={s.valblock}>
              location <span className={s.datavalue}>{this.state.account.location}</span>
            </div>
            <div className={s.valblock}>
              created <span className={s.datavalue}>{this.state.account.created}</span>
            </div>
          </div>
        </div>
        <div className="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-phone">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Payments</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div className={s.balance}>
              balance <span className={s.datavalue}>{this.state.account.balance}</span>
            </div>
            <div className={s.periodic_charge}>
              periodic_charge <span className={s.datavalue}>{this.state.account.periodic_charge}</span> (<span className={s.next_charge}>{this.state.account.next_charge}</span>)
            </div>
            <div className={s.credit_limit}>
              credit_limit <span className={s.datavalue}>{this.state.account.credit_limit}</span>
            </div>
            <div className={s.valblock}>
              tariff_id <span className={s.datavalue}>{this.state.account.tariff_id}</span>
            </div>
            <div className={s.valblock}>
              tariff_name <span className={s.datavalue}>{this.state.account.tariff_name}</span>
            </div>
            <div className={s.valblock}>
              support_bank_card <span className={s.datavalue}><i className={((this.state.account.support_bank_card)?s.okc:s.errc)+" material-icons"}>{(this.state.account.support_bank_card)?"check":"close"}</i></span>
            </div>
            <div className={s.valblock}>
              support_invoice <span className={s.datavalue}><i className={((this.state.account.support_invoice)?s.okc:s.errc)+" material-icons"}>{(this.state.account.support_invoice)?"check":"close"}</i></span>
            </div>
            <div className={s.valblock}>
              support_robokassa <span className={s.datavalue}><i className={((this.state.account.support_robokassa)?s.okc:s.errc)+" material-icons"}>{(this.state.account.support_robokassa)?"check":"close"}</i></span>
            </div>
          </div>
        </div>
        <div className="demo-card-wide mdl-card mdl-shadow--2dp mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell--12-col-tablet">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Notifications</h2>
          </div>
          <div className="mdl-card__supporting-text">
            <div className={s.valblock}>
              account_notifications <span className={s.datavalue}><i className={((this.state.account.account_notifications)?s.okc:s.errc)+" material-icons"}>{(this.state.account.account_notifications)?"check":"close"}</i></span>
            </div>
            <div className={s.valblock}>
              min_balance_to_notify <span className={s.datavalue}>{this.state.account.min_balance_to_notify}</span>
            </div>
            <div className={s.valblock}>
              news_notifications <span className={s.datavalue}><i className={((this.state.account.news_notifications)?s.okc:s.errc)+" material-icons"}>{(this.state.account.news_notifications)?"check":"close"}</i></span>
            </div>
            <div className={s.valblock}>
              send_js_error <span className={s.datavalue}><i className={((this.state.account.send_js_error)?s.okc:s.errc)+" material-icons"}>{(this.state.account.send_js_error)?"check":"close"}</i></span>
            </div>
            <div className={s.valblock}>
              tariff_changing_notifications <span className={s.datavalue}><i className={((this.state.account.tariff_changing_notifications)?s.okc:s.errc)+" material-icons"}>{(this.state.account.tariff_changing_notifications)?"check":"close"}</i></span>
            </div>
          </div>
        </div>
        <div className="mdl-cell mdl-cell--12-col">
          <h2 className="mdl-card__title-text">Additional info</h2>
          <div className="mdl-tabs mdl-js-tabs mdl-js-ripple-effect">
            <div className="mdl-tabs__tab-bar">
              <a href="#history-panel" className="mdl-tabs__tab">History</a>
              <a href="#phonenumbers-panel" className="mdl-tabs__tab"> Phone Numbers</a>
              <a href="#users-panel" className="mdl-tabs__tab">Users</a>
              <a href="#documents-panel" className="mdl-tabs__tab is-active">Documents</a>
              <a href="#aips-panel" className="mdl-tabs__tab">Authorized IPs</a>
              <a href="#scenarios-panel" className="mdl-tabs__tab">IDE (App+scenarios)</a>

            </div>
            <div className="mdl-tabs__panel" id="history-panel">
              <ShowHistory clientId={this.state.account.account_id} apiKey={this.state.account.api_key}/>
            </div>
            <div className="mdl-tabs__panel" id="phonenumbers-panel">
              <ShowNumbers clientId={this.state.account.account_id} apiKey={this.state.account.api_key}/>
            </div>
            <div className="mdl-tabs__panel" id="users-panel">
              <ShowUsers clientId={this.state.account.account_id} apiKey={this.state.account.api_key}/>
            </div>
            <div className="mdl-tabs__panel is-active" id="documents-panel">
              <ShowDocuments clientId={this.state.account.account_id} apiKey={this.state.account.api_key}/>
            </div>
            <div className="mdl-tabs__panel" id="aips-panel">
              <ShowAIPs clientId={this.state.account.account_id} apiKey={this.state.account.api_key}/>
            </div>
            <div className="mdl-tabs__panel " id="scenarios-panel">

            </div>
          </div>
        </div>
        <FabButtonBlock buttons={[{icon:'content_copy',nav:'/clone/'+this.props.child_id+'/'},{icon:'edit',nav:'/edit/'+this.props.child_id+'/'}]}/>

      </div>
    return(<div ref="root">
      {ctx}
      <div id="action-toast" className="mdl-js-snackbar mdl-snackbar">
        <div className="mdl-snackbar__text"/>
        <button className="mdl-snackbar__action" type="button"/>
      </div>
    </div>)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubAccShow);
