/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './ShowScenarios.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';


class ShowScenarios extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: false,
      page: 0,
      totalPages:1,
      scenarios: [],
      current_scenario_script:'// Start codding here to create new scenario',
      current_scenario_name:'',
      current_scenario_id:-1,
      page_edited:false,
    }
  }

  static propTypes = {
    clientId:React.PropTypes.number.isRequired,
    apiKey:React.PropTypes.string.isRequired
  };

  updateData(){
    this.setState({loading:true});
    fetch("https://api.voximplant.com/platform_api/GetScenarios/?"+
      "account_id=" + this.props.clientId +
      "&api_key=" + this.props.apiKey +
      "&count=500"+
      "&offset="+(20*this.state.page)
    )
      .then((response)=>{
        return response.text();
      })
      .then((body)=> {
        let response = JSON.parse(body);
        if(typeof (response.result)!="undefined"){
           this.setState({loading:false,scenarios:response.result,totalPages:(Math.ceil(response.total_count/20))});
        }
      })
  }



  componentDidMount() {
    this.updateData();
    window.componentHandler.upgradeElements(this.refs.root);
    this.initMonaco();
  }

  componentWillUnmount(){
    window.componentHandler.downgradeElements(this.refs.root);
    this.destroyMonaco();
  }
  initMonaco(){
    window.require(['vs/editor/editor.main'], () => {
      if(typeof monaco != "undefined") {
        this.editor = monaco.editor.create(document.getElementById('monaco_container'), {
          value: this.state.current_scenario_script,
          language: 'javascript'
        });
        this.editor.onDidChangeModelContent((e)=> {
          this.setState({page_edited: true, current_scenario_script: event.target.value})
        });
      }
    });
  }
  destroyMonaco(){
    if(typeof this.editor!="undefined")
      this.editor.destroy();
  }

  loadScript(scenario_id){
    fetch("https://api.voximplant.com/platform_api/GetScenarios/?"+
      "account_id=" + this.props.clientId +
      "&api_key=" + this.props.apiKey +
      "&scenario_id=" + scenario_id +
      "&with_script=true"
    )
    .then((response)=>{
      return response.text();
    })
    .then((body)=> {
      let response = JSON.parse(body);
      if(typeof (response.result)!="undefined"){
        this.setState({
          loading:false,
          current_scenario_script:response.result[0].scenario_script,
          current_scenario_name:response.result[0].scenario_name,
          current_scenario_id:scenario_id
        });
        this.editor.setValue(this.state.current_scenario_script);
      }
    })
  }
  loadEmpty(){
    this.setState({current_scenario_script:'// Start codding here to create new scenario', current_scenario_name:'', current_scenario_id:-1});
    this.editor.setValue('// Start codding here to create new scenario');
  }

  saveScenario(){
    if(this.state.current_scenario_id==-1){

    }
  }

  render(){
    return(<div ref="root" className={s.editors}>
      {(()=>{
        if(this.state.loading){
          return <div className="mdl-progress mdl-js-progress mdl-progress__indeterminate" style={{width:'100%'}}></div>
        }
      })()}
      <div className="mdl-grid" style={{marginTop:"0px",marginBottom:"0px"}}>
        <div className="mdl-cell mdl-cell--3-col">

        </div>
        <div className="mdl-cell mdl-cell--9-col">
          <div className={"mdl-textfield mdl-js-textfield mdl-textfield--floating-label"+((this.state.current_scenario_name.length>0)?" is-dirty":'')}>
            <input className="mdl-textfield__input" type="text" id="scenario_title" value={this.state.current_scenario_name} onChange={(e)=>{this.setValue({current_scenario_name:e.target.value})}}/>
            <label className="mdl-textfield__label" htmlFor="scenario_title">Scenario title</label>
          </div>
          <div className={s.prettyprint} id="monaco_container">

          </div>
        </div>
      </div>
    </div>);
  }
}

export default ShowScenarios;
