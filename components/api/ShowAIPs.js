/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './ShowAIPs.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';


class ShowAIPs extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: false,
      page: 0,
      totalPages:1
    }
  }

  static propTypes = {
    clientId:React.PropTypes.number.isRequired,
    apiKey:React.PropTypes.string.isRequired
  };

  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
  }

  componentWillUnmount(){
    window.componentHandler.downgradeElements(this.refs.root);
  }
  render(){
    return(<div ref="root">

    </div>);
  }
}

export default ShowAIPs;
