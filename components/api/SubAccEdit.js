/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './SubAccAdd.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';

function mapStateToProps(state,ownProps) {
  return { auth: state.auth,
    auth_error:state.auth_error,
    auth_msg:state.auth_msg,
    session_id:state.session_id,
    api_key:state.api_key,
    account_id:state.account_id
  };
}


const mapDispatchToProps = (dispatch,ownProps) => {
  return {
    logouted:()=>{
      dispatch({type:"LOGOUT"})
    }
  }
};


class SubAccEdit extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: true,
      loading_ok: false,
      loading_result: {},
      show_link:'/show',
      new_child_account_email: '',
      account_notifications: false,
      active: false,
      language_code: 'en',
      location: 0,
      min_balance_to_notify: 0,
      news_notifications: false,
      support_bank_card: false,
      support_invoice: false,
      support_robokassa: false,
      tariff_changing_notifications: false
    }
  }
  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
    fetch("https://api.voximplant.com/platform_api/GetChildrenAccounts/?" +
      "account_id="+this.props.account_id+
      "&child_account_id="+this.props.child_id+
      "&session_id="+this.props.session_id)
      .then((response)=>{
        return response.text();
      })
      .then((body)=>{
        let response = JSON.parse(body);
        if(typeof(response.error)!="undefined"){
          if(response.error.code==100){
            this.props.logouted();
          }
        }else{
          if(response.count==1){
            let item = response.result[0];
            console.log(item)
            this.setState({
              loading:false,
              new_child_account_email: item.account_email,
              account_notifications: item.account_notifications,
              active: item.active,
              language_code: item.language_code,
              location: item.location,
              min_balance_to_notify: item.min_balance_to_notify,
              news_notifications: item.news_notifications,
              support_bank_card: item.support_bank_card,
              support_invoice: item.support_invoice,
              support_robokassa: item.support_robokassa,
              tariff_changing_notifications: item.tariff_changing_notifications,
            });
          }
        }

      })
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  editAcc(){
    this.setState({loading:true});
    fetch("https://api.voximplant.com/platform_api/SetChildAccountInfo/?"+
      "account_id=" + this.props.account_id +
      "&session_id=" + this.props.session_id+
      "&child_account_id=" + this.props.child_id+
      "&new_child_account_email="+this.state.new_child_account_email+
      "&account_notifications="+this.state.account_notifications+
      "&active="+this.state.active+
      "&language_code="+this.state.language_code+
      "&location="+this.state.location+
      "&min_balance_to_notify="+this.state.min_balance_to_notify+
      "&news_notifications="+this.state.news_notifications+
      "&support_bank_card="+this.state.support_bank_card+
      "&support_invoice="+this.state.support_invoice+
      "&support_robokassa="+this.state.support_robokassa+
      "&tariff_changing_notifications="+this.state.tariff_changing_notifications
      )
      .then((response)=>{
        return response.text();
      })
      .then((body)=> {
        let response = JSON.parse(body);
        if(typeof(response.error)!="undefined"){
          this.setState({loading_result:response,loading:false});
          if(response.error.code==100){
            this.props.logouted();
          }
        }else{
          this.setState({loading_result:response,loading:false,loading_ok:true,show_link:'/show/'+this.props.account_id});
        }
      });
  }

  render(){
    let display={display:'flex'};
    let form ='';
    let error = '';
    if(this.state.loading_ok){
      form = <div>Create child account was successful. Now you can go to <Link to="/">child account list</Link> or to <Link to={this.state.show_link}>newly created child account</Link></div>
      display.display = 'none';
    }else if(this.state.loading){
      form = <div className={s.loadingWait}>
        <div id="progressbar" className="mdl-progress mdl-js-progress mdl-progress__indeterminate" style={{width:"100%"}}></div>
        Please, wait a few minutes.
      </div>;
      display.display = 'none';
    }else{
      if(typeof (this.state.loading_result.error)!="undefined")
        error = <div className={s.totallErr}>{this.state.loading_result.error.msg}</div>;
    }
    return (<div ref="root">
      {form}
      <div className="mdl-grid" style={display}>
        <div className="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone mdl-cell--2-offset-desktop ">
           {error}
             <div className={s.input}>
               <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <input className="mdl-textfield__input" type="email" id="account_email" value={this.state.new_child_account_email} onChange={(e)=>{this.setState({new_child_account_email: e.target.value})}}/>
                 <label className="mdl-textfield__label" htmlFor="account_email">Account email</label>
               </div>
             </div>
             <div className={s.input}>
               <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <input className="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="location" value={this.state.location} onChange={(e)=>{this.setState({location: e.target.value})}}/>
                 <label className="mdl-textfield__label" htmlFor="location">GTM offset</label>
                 <span className="mdl-textfield__error">Input is not a number!</span>
               </div>
             </div>
             <div className={s.input}>
               <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <input className="mdl-textfield__input" type="text" pattern="[a-z]{2}" id="language_code" value={this.state.language_code} onChange={(e)=>{this.setState({language_code: e.target.value})}}/>
                 <label className="mdl-textfield__label" htmlFor="language_code">language_code</label>
                 <span className="mdl-textfield__error">The notification language code (2 symbols, ISO639-1). Defaults to en</span>
               </div>
             </div>
             <div className={s.input}>
               <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                 <input className="mdl-textfield__input" type="text" pattern="-?[0-9]*(\.[0-9]+)?" id="min_balance_to_notify" value={this.state.min_balance_to_notify} onChange={(e)=>{this.setState({min_balance_to_notify: e.target.value})}}/>
                 <label className="mdl-textfield__label" htmlFor="min_balance_to_notify">The min balance value to notify.</label>
                 <span className="mdl-textfield__error">Input is not a number!</span>
               </div>
             </div>
        </div>
        <div className="mdl-cell mdl-cell--4-col mdl-cell--12-col-phone">
          <h4>Additional flags</h4>
          <div className={s.check_select}>
            <label className={((this.state.active)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="active">
              <input type="checkbox" id="active" className="mdl-switch__input" checked={this.state.active} onChange={(e)=>{this.setState({active: e.target.checked})}}/>
              <span className="mdl-switch__label">Active</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.account_notifications)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="account_notifications">
              <input type="checkbox" id="account_notifications" className="mdl-switch__input" checked={this.state.account_notifications} onChange={(e)=>{this.setState({account_notifications: e.target.checked})}}/>
              <span className="mdl-switch__label">Account notifications</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.tariff_changing_notifications)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="tariff_changing_notifications">
              <input type="checkbox" id="tariff_changing_notifications" className="mdl-switch__input" checked={this.state.tariff_changing_notifications} onChange={(e)=>{this.setState({tariff_changing_notifications: e.target.checked})}}/>
              <span className="mdl-switch__label">Tariff changing notifications</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.news_notifications)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="news_notifications">
              <input type="checkbox" id="news_notifications" className="mdl-switch__input" checked={this.state.news_notifications} onChange={(e)=>{this.setState({news_notifications: e.target.checked})}}/>
              <span className="mdl-switch__label">News notifications</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.support_robokassa)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="support_robokassa">
              <input type="checkbox" id="support_robokassa" className="mdl-switch__input" checked={this.state.support_robokassa} onChange={(e)=>{this.setState({support_robokassa: e.target.checked})}}/>
              <span className="mdl-switch__label">Support Robokassa</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.support_bank_card)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="support_bank_card">
              <input type="checkbox" id="support_bank_card" className="mdl-switch__input" checked={this.state.support_bank_card} onChange={(e)=>{this.setState({support_bank_card: e.target.checked})}}/>
              <span className="mdl-switch__label">Support Bank Card</span>
            </label>
          </div>
          <div className={s.check_select}>
            <label className={((this.state.support_invoice)?'is-checked':'')+" mdl-switch mdl-js-switch mdl-js-ripple-effect"} htmlFor="support_invoice">
              <input type="checkbox" id="support_invoice" className="mdl-switch__input" checked={this.state.support_invoice} onChange={(e)=>{this.setState({support_invoice: e.target.checked})}}/>
              <span className="mdl-switch__label">Support Invoice</span>
            </label>
          </div>
          <div className={s.docref}>
            Want to know more?
            <br/>
            Read documentation of VoxImplant HTTP API <a href="http://voximplant.com/docs/references/httpapi/#toc-setchildaccountinfo" target="_blank">SetChildAccountInfo</a> method.
          </div>
          <div>
            <button className="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" onClick={()=>this.editAcc()}>
              Set child account info
            </button>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SubAccEdit);
