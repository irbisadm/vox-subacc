/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import s from './ShowDocuments.css';
import {connect} from 'react-redux';
import { Link } from 'react-app';


class ShowDocuments extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      loading: false,
      page: 0,
      totalPages:1,
      country_code:'',
      phone_category_name:'',
      phone_region_code:'',
      verified:false,
      in_progress:false,
      country:[],
      category:[],
      region:[]
    }
    this.generator = this.getData();
  }

  static propTypes = {
    clientId:React.PropTypes.number.isRequired,
    apiKey:React.PropTypes.string.isRequired
  };

  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
  }

  componentWillUnmount(){
    window.componentHandler.downgradeElements(this.refs.root);
  }


  render(){
    return(<div ref="root">
      <div className="mdl-grid">
        <div className="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--12-col">
        </div>
        <div className="mdl-cell mdl-cell--9-col">
        </div>
        <div className="mdl-card mdl-shadow--2dp mdl-cell mdl-cell--3-col">
          <div className="mdl-card__title">
            <h2 className="mdl-card__title-text">Filter</h2>
          </div>
          <div className="mdl-card__supporting-text" style={{overflow: "visible"}}>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
              <input className="mdl-textfield__input" type="text" id="country_code-input" value="Belarus" readOnly tabIndex="-1"/>
              <label htmlFor="country_code-input">
                <i className="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
              </label>
              <label htmlFor="country_code-input" className="mdl-textfield__label">Country code</label>
              <ul htmlFor="country_code-input" className="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                <li className="mdl-menu__item">Germany</li>
                <li className="mdl-menu__item">Belarus</li>
                <li className="mdl-menu__item">Russia</li>
              </ul>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
              <input className="mdl-textfield__input" type="text" id="phone_category_name-input" value="Belarus" readOnly tabIndex="-1"/>
              <label htmlFor="phone_category_name-input">
                <i className="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
              </label>
              <label htmlFor="phone_category_name-input" className="mdl-textfield__label">Phone category name</label>
              <ul htmlFor="phone_category_name-input" className="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                <li className="mdl-menu__item">Germany</li>
                <li className="mdl-menu__item">Belarus</li>
                <li className="mdl-menu__item">Russia</li>
              </ul>
            </div>
            <div className="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select">
              <input className="mdl-textfield__input" type="text" id="phone_region_code-input" value="Belarus" readOnly tabIndex="-1"/>
              <label htmlFor="phone_region_code-input">
                <i className="mdl-icon-toggle__label material-icons">keyboard_arrow_down</i>
              </label>
              <label htmlFor="phone_region_code-input" className="mdl-textfield__label">Phone region code</label>
              <ul htmlFor="phone_region_code-input" className="mdl-menu mdl-menu--bottom-left mdl-js-menu">
                <li className="mdl-menu__item">Germany</li>
                <li className="mdl-menu__item">Belarus</li>
                <li className="mdl-menu__item">Russia</li>
              </ul>
            </div>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="verified-input">
              <input type="checkbox"
                     id="verified-input"
                     className="mdl-switch__input"
                     checked={this.state.verified}
                     onChange={(e)=>{this.setState({verified:e.target.checked})}}
              />
              <span className="mdl-switch__label">Verified</span>
            </label>
            <label className="mdl-switch mdl-js-switch mdl-js-ripple-effect" htmlFor="in_progress-input">
              <input type="checkbox"
                     id="in_progress-input"
                     className="mdl-switch__input"
                     checked={this.state.in_progress}
                     onChange={(e)=>{this.setState({in_progress:e.target.checked})}}
              />
              <span className="mdl-switch__label">In progress</span>
            </label>
          </div>
          <div className="mdl-card__actions mdl-card--border">
            <button className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onClick={()=>{console.error(this.generator.next())}}>
              Apply filter
            </button>
            <button className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" onClick={()=>{this.generator = this.getData()}}>
              Apply filter
            </button>
          </div>
        </div>
      </div>
    </div>);
  }
}

export default ShowDocuments;
