/**
 *
 */
import React from 'react';
import s from './FabButtonBlock.css';
import { Link } from 'react-app';

export class FabButtonBlock extends React.Component {
  constructor(props,context,updater){
    super(props,context,updater);
    this.state = {
      opened: false,
    }
  }

  static PropTypes = {
    buttons:React.PropTypes.object.isRequired
  };

  componentDidMount() {
    window.componentHandler.upgradeElements(this.refs.root);
  }

  componentWillUnmount() {
    window.componentHandler.downgradeElements(this.refs.root);
  }

  render() {
    if(!Array.isArray(this.props.buttons)) {
      return (
        <div className={s.fabContainer} ref="root">
          <div className={s.fabMain}>
            <Link className="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" to={this.props.buttons.nav}>
              <i className="material-icons">{this.props.buttons.icon}</i>
            </Link>
          </div>
        </div>);

    }else if(this.props.buttons.length<3){
      return (
        <div className={s.fabContainer} ref="root">
          {this.props.buttons.map((result,index)=>{
            return <div className={s.fabNormal} key={result.nav}>
              <Link className={((index==1)?"mdl-button--colored":'')+" mdl-button mdl-js-button mdl-button--fab"}  to={result.nav}>
                <i className="material-icons">{result.icon}</i>
              </Link>
            </div>})
          }
        </div>)
    }else{
      return (
        <div className={s.fabContainer} ref="root">
          <div className={(this.state.opened)?s.fabHolderOpenned:s.fabHolderHidden}>
            {this.props.buttons.map((result,index)=>{
              return  <button
                  className={s.fabMini + " mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-button--colored"}
                  key={result.nav} to={result.nav}>
                  <i className="material-icons">{result.icon}</i>
                </button>
            })}
          </div>
          <div className={s.fabMain}>
            <button className="mdl-button mdl-js-button mdl-button--fab mdl-button--colored" onClick={()=>{this.setState({opened:!this.state.opened})}}>
              <i className="material-icons">{(this.state.opened)?"close":"menu"}</i>
            </button>
          </div>
        </div>)
    }

  }
}

