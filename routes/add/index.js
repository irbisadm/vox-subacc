/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { Layout,SubAccAdd } from '../../components';

function AddPage({ title }) {
  return (
    <Layout>
      <h1>{title}</h1>
      <SubAccAdd></SubAccAdd>
    </Layout>
  );
}

AddPage.propTypes = {
  title: React.PropTypes.string.isRequired,
};

export default {

  path: '/add',

  action() {
    return {
            title: 'Add new child account',
            component: AddPage,
            props: {title:'Add new child account'},
          }
  },

};
