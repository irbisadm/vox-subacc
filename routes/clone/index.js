/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { Layout,SubAccClone } from '../../components';

function ClonePage({ title, child_id }) {
  return (
    <Layout>
      <h1>{title}</h1>
      <SubAccClone  child_id={child_id}></SubAccClone>
    </Layout>
  );
}

ClonePage.propTypes = {
  title: React.PropTypes.string.isRequired,
  child_id: React.PropTypes.string.isRequired,
};

export default {

  path: '/clone/:child_id',

  action(context) {
    return {
            title: 'Clone child account  ID:'+context.params.child_id,
            component: ClonePage,
            props: {title:'Clone child account ID:'+context.params.child_id,child_id:context.params.child_id},
          }
  },

};
