/**
 * React Static Boilerplate
 * https://github.com/koistya/react-static-boilerplate
 *
 * Copyright © 2015-2016 Konstantin Tarkus (@koistya)
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE.txt file in the root directory of this source tree.
 */

import React from 'react';
import { Layout,SubAccTable } from '../../components';

function IndexPage({ title }) {
  return (
    <Layout>
      <h1>{title}</h1>
      <SubAccTable></SubAccTable>
    </Layout>
  );
}

IndexPage.propTypes = {
  title: React.PropTypes.string.isRequired,
};

export default {

  path: '/',

  action() {
    return {
            title: 'List of child accounts',
            component: IndexPage,
            props: {title:'List of child accounts'},
          }
  },

};
